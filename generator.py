import json
import os
from collections import OrderedDict

from sqlalchemy import create_engine

import fakedata


generator = fakedata.Generator()

with open('nepplaatsen.json') as jsonfile:
    config = json.load(jsonfile, object_pairs_hook=OrderedDict)

dataname = config['name']


# setup outputs
if "csv" in config['outputs']:
    # check if dir exists
    if not os.path.exists(dataname):
        os.makedirs(dataname)
if "sqlite" in config['outputs']:
    engine = create_engine('sqlite:///%s.db' % dataname)


# create tables
for table in config['tables']:
    tablename = table['name']
    if 'number' in table:
        number = table['number']
    print "generating " + tablename
    # construct table
    data = []
    for column, options in table['columns'].iteritems():
        if 'number' in options:
            number = options['number']
        if column == 'phonenumbers':
            data.append(generator.phone_numbers(n=number))
        if column == 'connections':
            data.append(generator.phone_connections(n=number))
        if column == 'locations':
            data.append(generator.locations(n=number, startingpoint=options['startingpoint'], radius=.2))
        if column == 'names':
            data.append(generator.names(n=number))
        if column == 'addresses':
            data.append(generator.addresses(n=number))
        if column == 'countries':
            data.append(generator.countries(n=number, repeat=options['repeat']))

    output = fakedata.Output(*data)
    if "csv" in config['outputs']:
        print "writing %s to %s.csv" % (tablename, tablename)
        output.to_csv(os.path.join(dataname, tablename+".csv"))

    if "sqlite" in config['outputs']:
        print "writing %s to sqlite" % (tablename)
        output.to_sqlite(tablename, engine)
