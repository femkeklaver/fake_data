import pandas as pd

import fakedata


class Generator(object):
    def __init__(self, columns):
        self.generator = fakedata.Generator()

        self.columns = columns
        self.companies = None
        self.cities = None

    def generate(self, csv_in, csv_out):
        df = pd.read_csv(csv_in, sep="\t")
        for column, datatype in self.columns.iteritems():
            if column not in df:
                continue
            content = df[column]
            c = list(set(content))
            n = len(c)
            if datatype == 'company':
                if self.companies:
                    print self.companies
                    # check if all items in self.companies
                    known_companies = self.companies.keys()
                    unknown = [x for x in c if x not in known_companies]
                    fake_content = [x[0] for x in self.generator.companies(n=len(unknown)).values]
                    self.companies.update(dict((source, target) for source, target in zip(unknown, fake_content)))
                else:
                    fake_content = [x[0] for x in self.generator.companies(n=n).values]
                    self.companies = dict((source, target) for source, target in zip(c, fake_content))
                df[column] = [self.companies[source] for source in content]
            if datatype == 'city':
                if self.cities:
                    # check if all items in self.cities
                    known_cities = self.cities.keys()
                    unknown = [x for x in c if x not in known_cities]
                    fake_content = [x[0] for x in self.generator.cities(n=len(unknown)).values]
                    self.cities.update(dict((source, target) for source, target in zip(unknown, fake_content)))
                else:
                    fake_content = [x[0] for x in self.generator.cities(n=n).values]
                    self.cities = dict((source, target) for source, target in zip(c, fake_content))
                df[column] = [self.cities[source] for source in content]
                # fake_content = [x[0] for x in self.generator.cities(n=n).values]
                # self.cities = dict((source, target) for source, target in zip(c, fake_content))
            # df[column] = [convert[source] for source in content]

        df.to_csv(csv_out, index=False, encoding='utf-8', sep='\t')


if __name__ == "__main__":
    # columns as dict
    csv_in = ""
    csv_out = ""
    columns = {'stroom': 'company', 'doel': 'city', 'bron': 'company', 'naam': 'company'}
    generator = Generator(columns)
    generator.generate("/home/femke/Documents/ingaande_stromen.csv", "/home/femke/Documents/ingaande_stromen_new.csv")
    generator.generate("/home/femke/Documents/uitgaande_stromen.csv", "/home/femke/Documents/uitgaande_stromen_new.csv")
    generator.generate("/home/femke/Documents/transacties.csv", "/home/femke/Documents/transacties_new.csv")
