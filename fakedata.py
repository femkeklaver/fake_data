import numpy as np
import pandas as pd
from faker import Factory
from geopy import Nominatim


# fake = Factory.create('nl_NL')
#
# numbers = [fake.phone_number() for _ in range(25)]

# connections = np.random.choice(numbers, (150, 2))
# df = pd.DataFrame(connections)
#
# # df.to_sql('connection', engine)
#
#
# geolocator = Nominatim(domain='kc-geo.holmes.nl:8080', scheme='http')


class Generator(object):
    """Fake data generators that generate n instances of fake data

    """

    def __init__(self, location='nl_NL'):
        self.fake = Factory.create(location)
        # self.geolocator = Nominatim(domain='kc-geo.holmes.nl:8080', scheme='http')
        self.geolocator = Nominatim()
        self.numbers = []
        self.worldcities = []
        self.n = 10

    def phone_numbers(self, n, replace=False, empty_values=False):
        numbers = [self.fake.phone_number() for _ in range(n)]
        self.numbers = numbers
        return pd.DataFrame(np.array(numbers), columns=["phonenumber"])

    def locations(self, n, startingpoint, radius=0.01, empty_values=False):
        locs = []
        for _ in range(n):
            center = self.geolocator.geocode(startingpoint)
            fake_lat = self.fake.geo_coordinate(center=center.latitude, radius=radius)
            fake_lon = self.fake.geo_coordinate(center=center.longitude, radius=radius)
            # reverse geocode to avoid water
            location = self.geolocator.reverse((fake_lat, fake_lon), timeout=999)
            locs.append([location.latitude, location.longitude])
            if _ % 10 == 0 and _ > 0:
                print "faking location %i of %i" % (_, n)
        return pd.DataFrame(np.array(locs), columns=["lat", "lon"])

    def phone_connections(self, n, use_existing=True):
        if not self.numbers or use_existing is False:
            self.phone_numbers(n, replace=True)
        weights = np.random.dirichlet(np.ones(len(self.numbers)), size=1)[0]

        return pd.DataFrame(np.random.choice(self.numbers, (n, 2), p=weights),
                            columns=["from", "to"])

    def names(self, n):
        names = [unicode(self.fake.name()) for _ in range(n)]
        return pd.DataFrame(names, columns=["name"])

    def addresses(self, n):
        addresses = [unicode(self.fake.address()) for _ in range(n)]
        return pd.DataFrame(addresses, columns=["address"])

    def companies(self, n):
        # companies = [unicode(self.fake.company()) for _ in range(n)]
        # make cool companies

        prefixes = ['i', 'Green ', 'Mega ', 'Super ', 'Omni ', 'e', 'Hyper ', 'Global ', 'Vital ',
                  'Next ', 'Pacific ', 'Metro ', 'Unity ', 'G-', 'Trans ', 'Infinity ', 'Superior ',
                  'Monolith ', 'Best ', 'Atlantic ', 'First ', 'Union ', 'National ']

        centers = ['Biotic', 'Info', 'Data', 'Solar', 'Aerospace', 'Motors', 'Nano', 'Online',
                  'Circuits', 'Energy', 'Med', 'Robotic', 'Exports', 'Security', 'Systems',
                  'Financial', 'Industrial', 'Media', 'Materials', 'Foods', 'Networks', 'Shipping',
                  'Tools', 'Medical', 'Publishing', 'Enterprises', 'Audio', 'Health', 'Bank',
                  'Imports', 'Apparel', 'Petroleum', 'Studios']

        suffixes = [' B.V.', ' V.O.F.', ' N.V']
        companies = []
        for _ in range(n):
            name = ""
            if np.random.choice([True, False], p=[.7, .3]):
                name += np.random.choice(prefixes)
                p = True
            name += np.random.choice(centers)
            if np.random.choice([True, False], p=[.9, .1]) or not p:
                name += np.random.choice(suffixes, p=[.7, .2, .1])
            companies.append(name)

        return pd.DataFrame(companies, columns=["companies"])

    def countries(self, n, repeat=False):
        countries = [unicode(self.fake.country()) for _ in range(n)]
        # print countries
        if repeat == "True":
            weights = np.random.dirichlet(np.ones(n), size=1)[0]
            countries = np.random.choice(countries, n, p=weights)
        return pd.DataFrame(countries, columns=["countries"])

    def countrycodes(self, n, repeat=False):
        countries = [unicode(self.fake.country_code()) for _ in range(n)]
        # print countries
        if repeat == "True":
            weights = np.random.dirichlet(np.ones(n), size=1)[0]
            countries = np.random.choice(countries, n, p=weights)
        self.countrycodes = countries
        return pd.DataFrame(countries, columns=["countries"])

    def cities(self, n, repeat=False):
        cities = [unicode(self.fake.city()) for _ in range(n)]
        return pd.DataFrame(cities, columns=["cities"])

    def international_cities(self, n, countrycodes=[]):
        if countrycodes:
            self.countrycodes = countrycodes
        if not self.worldcities:
            self.worldcities = pd.read_csv('resources/worldcities.csv', usecols=[0, 6],
                                           error_bad_lines=False, header=0,
                                           names=['countrycode', 'countryname'])
        if self.countrycodes:
            cities = self.worldcities.query('countrycode == %s' % self.countrycodes)
        else:
            raise Exception("Doesn't work without countrycodes. ")
        result = []
        for country in self.countrycodes:
            countrycities = cities.query('countrycode == "%s"' % country)
            if countrycities.empty:
                print 'countrycode == "%s"' % country
                print country
            result.append(np.random.choice(countrycities['countryname'], 1))

        return pd.DataFrame(result, columns=["cities"])


class Output(object):
    def __init__(self, *data):
        self.df = pd.concat(data, axis=1)

    def to_csv(self, filename):
        self.df.to_csv(filename, index=False, encoding='utf-8', sep='\t')

    def to_sqlite(self, tablename, engine):
        self.df.to_sql(tablename, engine, index=False, if_exists='replace')


if __name__ == "__main__":
    generator = Generator()
    numbers = generator.phone_connections(50)
    locations = generator.locations(25, 'Amsterdam')
    print Output(locations).df
    # Output(locations).to_csv('/mnt/homedirs/femke/test_locations.csv')
